require 'etc'
require 'pathname'

def base
  @base ||= Pathname.new(__FILE__).realpath.dirname
end

def dot(file)
  home + ".#{file}"
end

def glob(pattern)
  Pathname.glob(base + pattern)
end

def home
  return @home if @home
  user = Etc.getlogin
  home = begin
           Dir.home(user)
         rescue NoMethodError
           ENV['HOME']
         end
  @home = Pathname.new(home)
end

def os
  @os ||= RUBY_PLATFORM =~ /linux/ ? 'linux' : 'darwin'
end

def permission(file)
  file.lstat.mode & 0777
end

def stripped(file)
  relative = file.relative_path_from(base)
  filename = relative.to_s.split('/')[1..-1].join('/')
  Pathname.new(filename)
end

namespace :dotfiles do
  task :setup do
    paths = glob("{all,#{os}}/**/*")
    @dirs = paths.select { |path| path.directory? and !path.symlink? }
    @files = paths.select(&:file?)
    @symlinks = paths.select(&:symlink?)
  end

  desc "Creates directories in #{home}"
  task :dirs => :setup do
    @dirs.each do |dir|
      homedir = dot(stripped(dir))
      mkdir homedir unless homedir.exist?
      chmod permission(dir), homedir
    end
  end

  desc "Links files to #{home}"
  task :files => :setup do
    @files.each do |file|
      homefile = dot(stripped(file))
      relative_path = file.expand_path.relative_path_from(homefile.dirname)
      ln_sf relative_path, homefile
    end
  end

  desc "Creates symbolic links to #{home}"
  task :symlinks => :setup do
    @symlinks.each do |symlink|
      homefile = dot(stripped(symlink))
      symlink_dir = symlink.dirname.expand_path
      symlink_path = symlink.readlink
      symlink_full_path = symlink_dir + symlink_path
      relative_path = symlink_full_path.relative_path_from(homefile.dirname)
      rm_f homefile if homefile.symlink? and homefile.directory?
      ln_sf relative_path, homefile
    end
  end
end

task :dotfiles => ['dotfiles:dirs', 'dotfiles:files', 'dotfiles:symlinks']

namespace :gitignore do
  desc "Generates the global gitignore file"
  task :global do
    files = glob('gitignore/**/*')
    contents = files.map { |file| file.read }
    local = base + 'gitignore.local'
    contents << local.read if local.exist?
    dotfile = dot('gitignore.global')
    dotfile.open('w') do |file|
      file.write contents.join("\n")
    end
    puts "Generated #{dotfile}"
  end
end

task :gitignore => ['gitignore:global']

task :default => [:dotfiles, :gitignore]
